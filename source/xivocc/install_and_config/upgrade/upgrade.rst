*******
Upgrade
*******

Overview
========

The following components will be upgraded :

- Docker images
- xivocc-installer package

.. warning:: This upgrade procedure applies only to XiVO CC installed via the ``xivocc-installer`` package.

Prerequisites
=============

Before upgrading you have to check or change your sources list.
It should be located in the file :file:`/etc/apt/sources.list.d/xivo-solutions.list`.

There are two cases :

#. Upgrade to *latest* version,
#. Upgrade to **a specific** version (or an archive version)

Upgrade to *latest* version
------------------------------

To upgrade to the latest version the sources list must point towards *debian* URI and *xivo-solutions* suite::

    deb http://mirror.xivo.solutions/debian/ xivo-solutions main


Upgrade to **specific** version
-------------------------------

To upgrade to a **specific** version the sources list must point towards *archive* URI and *xivo-solutions-VERSION* suite.

For example if you want to upgrade to **2016.03** version you should have::

    deb http://mirror.xivo.solutions/archive/ xivo-solutions-2016.03 main

Note the ``/archive/`` and ``-2016.03`` above.


Upgrade
=======

When you have checked the sources.list you can upgrade with the following commands::

    apt-get update
    apt-get install xivocc-installer

The current *docker-compose.yml* file will be renamed to *docker-compose.yml.dpkg-old* and new template downloaded.
A new *docker-compose.yml* file will be rendered from the template using the current xivocc version.

Then you run the new version by *dcomp up -d* eventually preceded by the *dcomp pull* to download the new images.


Upgrade notes
=============

2016.04
-------

Consult the `2016.04 Roadmap <https://projects.xivo.solutions/versions/8>`_

Parameters for ``/etc/docker/compose/docker-xivocc.yml`` are now stored in ``/etc/docker/compose/.env`` file. Important
parameter is ``XIVO_AMI_SECRET``, which holds Ami password.

.. note:: If you are using ``docker-compose`` instead of recommended alias ``dcomp``, make sure your current directory
          is  ``/etc/docker/compose``, otherwise ``/etc/docker/compose/.env`` won't be used.

2016.03
-------
No behavior changes.
