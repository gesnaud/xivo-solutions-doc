******
WebRTC
******

.. note:: added in version 2016.04

From version 2016.04 one can use WebRTC with *XiVO PBX* and *XiVO CC* in the following environment:

* LAN network (currently no support for WAN environment),
* with the:

  * *Web Assistant* with Chrome browser version 55 (tested on 55.0.2883.87 m 64-bit),
  * or *Desktop Assistant*


.. _webrtc_requirements:

WebRTC Requirements
===================

The **requirements** are:

* to have a microphone and headphones for your PC,
* to configure, in the *XiVO PBX*, a user with a WebRTC line (see: :ref:`configure_user_with_webrtc_line`),
* have a SSL/TLS certificate signed by a certification authority installed on the nginx of *XiVO CC*,
* and use *https*:

  * *Web Assistant*: you must connect to the *Web Assistant* via `https` protocol,
  * *Desktop Application*: you must check *Protocol -> Secure* in the application parameters.
      
.. note:: Currently you can not have a user configured for both WebRTC and a phone set at the same time.
