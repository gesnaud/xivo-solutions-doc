******
WebRTC
******

General notes
=============

.. note:: added in version 2016.04

From version 2016.04 one can use WebRTC with *XiVO PBX* and *XiVO CC* in the following environment:

* LAN network (currently no support for WAN environment),
* with the:

  * *Web Assistant* with Chrome browser version 55 (tested on 55.0.2883.87 m 64-bit),
  * or *Desktop Assistant*

See :ref:`webrtc_requirements`.

.. note:: Current WebRTC implementation requires to create user with one line configured for WebRTC. To have user with both SIP and WebRTC line is not supported.

.. _configure_user_with_webrtc_line:

Configuration of user with WebRTC line
======================================

1. Create user

2. Add line to user without any device

3. Edit the line created and, in the *Advanced* tab, add `webrtc=yes` options:

.. figure:: webrtc_line.png
    :scale: 100%


Manual configuration of user with WebRTC line
==============================================

For the records 

.. toctree::
    
    webrtc_manualconf

